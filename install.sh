#!/bin/bash

# this dir should be in your home with the same name "dotfiles" for this script to work.
ls ~/dotfiles | grep -v "install.sh\|config\|etc" | xargs -I %i  ln -fsn ~/dotfiles/%i ~/.%i
ln -fs ~/dotfiles/config/* ~/.config

# special cases
# FIXME updates keep overwriting the symlinks
sudo ln -fs ~/dotfiles/etc/NetworkManager/dispatcher.d /etc/NetworkManager

