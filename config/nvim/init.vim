" plugins {{{{
"curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.local/share/nvim/plugged')

Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'godlygeek/tabular'
Plug 'morhetz/gruvbox'

call plug#end()
"}}}}


" colorschemes {{{{
set background=dark    
let g:one_allow_italics = 1
colorscheme gruvbox 
hi Normal guibg=NONE ctermbg=NONE

" }}}}

" general mappings {{{{
command Sudo :execute ':silent w !sudo tee % > /dev/null' | :edit!
let mapleader=","
vmap <leader>r :!render<CR>
" }}}}

" general settings {{{{
filetype plugin on 
set foldmethod=marker
set foldmarker={{{{,}}}}
set autoindent
set hlsearch incsearch  smartcase
set spellcapcheck=    
" }}}}
