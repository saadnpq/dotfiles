(in-package :next)

(defvar *my-keymap* (make-keymap))
(define-key :keymap *my-keymap*
  "J" #'switch-buffer-next
  "K" #'switch-buffer-previous
  "b" #'switch-buffer
  "C-u" #'scroll-page-up
  "C-d" #'scroll-page-down
  )

(define-mode my-mode ()
  ""
  ((keymap-schemes :initform (list :emacs-map *my-keymap*
                                   :vi-normal *my-keymap*))))

(defvar *my-blocked-hosts*
  (next/blocker-mode:make-hostlist
   :hosts '("platform.twitter.com"
            "syndication.twitter.com"
            "youtube.com"
            "m.media-amazon.com")))

(defun my-buffer-defaults (buffer)
  (pushnew 'vi-normal-mode (default-modes buffer))
  (pushnew 'my-mode (default-modes buffer))
  (next/blocker-mode:blocker-mode
   :hostlists (list *my-blocked-hosts*)
   :buffer buffer)
  )

(defun my-interface-defaults ()
  (hooks:add-to-hook (hooks:object-hook *interface* 'buffer-make-hook)
                     #'my-buffer-defaults))
(hooks:add-to-hook '*after-init-hook* #'my-interface-defaults)
