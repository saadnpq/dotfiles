#{{{{ pip completion
function _pip_completion {
  local words cword
  read -Ac words
  read -cn cword
  reply=( $( COMP_WORDS="$words[*]" \
             COMP_CWORD=$(( cword-1 )) \
             PIP_AUTO_COMPLETE=1 $words[1] ) )
}
compctl -K _pip_completion pip
# pip zsh completion end }}}}

# ENV {{{{ 
export PATH=/home/saadnpq/scripts:/home/saadnpq/.gem/ruby/2.6.0/bin:$PATH  

export EDITOR=e
# export MANPAGER='vimpager'
# export PAGER='vimpager'
# alias less=$PAGER
# alias zless=$PAGER

export SATA_LINKPWR_ON_BAT=max_performance
export VPN_UDP=/home/saadnpq/programs/openvpn-2.4.6/1-ovpn-saadnpq/ovpn_udp
export VPN_TCP=/home/saadnpq/programs/openvpn-2.4.6/1-ovpn-saadnpq/ovpn_tcp
export XDG_CONFIG_HOME=$HOME/.config
export TS_MAXFINISHED=5

#theming
export QT_QPA_PLATFORMTHEME=qt5ct
export GTK_THEME=Adwaita

export SSH_KEY_PATH="~/.ssh/rsa_id"

#wayland specific 
# export _JAVA_AWT_WM_NONREPARENTING=1
# export QT_QPA_PLATFORM=wayland-egl
# export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
# export GDK_BACKEND=wayland 
# export CLUTTER_BACKEND=wayland 
# }}}}


# for ibus
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

# aliases {{{{

alias mlocal="ncmpcpp -h localhost -p 6600"                                                      
alias notes="nvim ~/notes/index.md"                                    
alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias r="ranger"                                                       
alias c="calcurse"                                                     
alias calcurse="calcurse -D ~/.config/calcurse"                                                     
alias youtube-dl-audio="youtube-dl --extract-audio --audio-format mp3 "             
alias fortune="fortune | lolcat"
alias tor="~/programs/tor-browser_en-US/start-tor-browser.desktop"
alias viless='/usr/share/nvim/runtime/macros/less.sh'
alias sudo='/bin/sudo'
alias su='/usr/bin/su'
alias tsp_videos='TS_SOCKET=/tmp/videos tsp'
alias serve='sudo python -m http.server 80'
alias ip='ip -color'
alias saadnpq='ssh root@saadnpq.com'
alias local.saadnpq='ssh root@local.saadnpq.com'


bindkey -v
source /usr/share/doc/pkgfile/command-not-found.zsh

source /usr/share/doc/pkgfile/command-not-found.zsh
export PATH=$HOME/scripts:$PATH

if [[ "$(tty)" == "/dev/tty1" ]] 
then 
    startx
fi
